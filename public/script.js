//Values extracted as constants for easier adjustment
const INSTALLMENT_PERCENTAGE = 0.1;
const MAX_LOAN_PERCENTAGE = 2; // 2 = 100%
const PAY_PER_WORK = 100;
const URL = "https://noroff-komputer-store-api.herokuapp.com/";
const FETCH_PATH = "computers";

//The list of laptops loaded from API
let laptops = [];
//Accounts
let bankBalance = 0;
let loan = 0;
let payBalance = 0;

//Simple selector to save some keystrokes
const $$ = (selector) => {
    return document.getElementById(selector);
}
//Easy access to Elements.
const Elements = {
    BankBalanceElement: $$('bank-balance'),
    PayBalanceElement: $$('pay-balance'),
    BankTransferButtonElement: $$('deposit-to-bank'),
    GetPaidButtonElement: $$('get-paid'),
    LoanContainerElement: $$('loan-container'),
    LoanOutStandingElement: $$('loan'),
    RepayLoanButtonElement: $$('repay-loan'),
    GetLoanElement: $$('get-loan'),
    LaptopsListElement: $$('laptops'),
    LaptopFeaturesListElement: $$('laptop-features'),
    LaptopImageElement: $$('laptop-image'),
    LaptopNameElement: $$('laptop-name'),
    LaptopDescriptionElement: $$('laptop-description'),
    LaptopPriceElement: $$('laptop-price'),
    BuyNowButtonElement: $$('buy-now-button'),
}

//The Renderer for DOM manipulation
const Renderer = {
    addLaptops(laptops) {
        laptops.forEach(l => {
            const laptopElement = document.createElement("option");
            laptopElement.value = l.id;
            laptopElement.appendChild(document.createTextNode(l.title));
            Elements.LaptopsListElement.appendChild(laptopElement);
        });
    },
    updatePayBalance() {
        Elements.PayBalanceElement.value = toCurrency(payBalance);
    },
    clearPayBalance() {
        Elements.PayBalanceElement.value = toCurrency(0);
    },
    updateBankBalance() {
        Elements.BankBalanceElement.value = toCurrency(bankBalance);
    },
    clearBankBalance() {
        Elements.BankBalanceElement.value = toCurrency(0);
    },
    updateLoanOutstanding() {
        Elements.LoanOutStandingElement.value = toCurrency(loan);
    },
    clearLoanOutstanding() {
        Elements.LoanOutStandingElement.value = toCurrency(0);
    },
    showLoanContainer() {
        Elements.LoanContainerElement.style.display = 'flex';
    },
    hideLoanContainer() {
        Elements.LoanContainerElement.style.display = 'none';
    },
    showRepayLoanButton() {
        Elements.RepayLoanButtonElement.hidden = false;
    },
    hideRepayLoanButton() {
        Elements.RepayLoanButtonElement.hidden = true;
    },
    updateLaptopFeatures(features) {
        Helpers.removeAllChildNodes(Elements.LaptopFeaturesListElement);
        features.forEach((x) => {
            let element = document.createElement('li');
            element.innerText = x;
            Elements.LaptopFeaturesListElement.appendChild(element);
        })
    },
    updateLaptopName(title) {
        Elements.LaptopNameElement.innerText = title;
    },
    updateLaptopDescription(description) {
        Elements.LaptopDescriptionElement.innerText = description;
    },
    updateLaptopPrice(price) {
        Elements.LaptopPriceElement.value = toCurrency(price);
    },
    updateLaptopImage(url) {
        Elements.LaptopImageElement.src = url;
    },
    async updateLaptopSelection(id) {
        try {
            const laptop = await laptops.find(x => x.id == id);

            Renderer.updateLaptopFeatures(laptop.specs)
            Renderer.updateLaptopImage(`${URL}${laptop.image}`);
            Renderer.updateLaptopName(laptop.title);
            Renderer.updateLaptopDescription(laptop.description);
            Renderer.updateLaptopPrice(laptop.price);
        } catch (error) {
            console.log(error);
        }
    }
}

//Retrieves the laptops from the provided API. Added fix for an image that wouldn't load.
async function GetLaptops() {
    try {
        let response = await fetch(`${URL}${FETCH_PATH}`);
        let data = await response.json();
        laptops = data;
        Renderer.addLaptops(laptops);
    } catch (error) {
        console.log(error)
    }

    //fix typo in laptop id 5 image type from .jpg to .png
    try {
        let secondResponse = await fetch('https://noroff-komputer-store-api.herokuapp.com/assets/images/5.jpg')
        if (secondResponse.status == 404) {
            laptops[4].image = 'assets/images/5.png';
            console.log("The url of the image in laptop 'The Visor' is broken. Fixing it to match proper url");
        }
    } catch (error) {
        console.log(error);
    }
}

    // Converts to the currency specified. 
    // Didnt put in Helpers class because wanted to save some keystrokes in the template literals.
function toCurrency(amount) {
    return new Intl.NumberFormat('sv-SE', {
        style: 'currency',
        currency: 'SEK'
    }).format(amount);
}
    // Helper class with helping methods
class Helpers {
    // Check if a number is numeric
    static isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
    // Removes all childnodes from the provided parentnode.
    static removeAllChildNodes(parent) {
        while (parent.firstChild) {
            parent.removeChild(parent.firstChild);
        }
    }
}
    // Warning: Be careful treading down this path, there are spaghetti monsters
    // All added Eventlisteners have a corresponding Event in this class.
class Events {
    //Adds pay per 'work' then renders textboxes to match.
    static getPaid() {
        payBalance += PAY_PER_WORK;
        Renderer.updatePayBalance();
    }
    // Transfers funds from pay balance to bank balance.
    // Checks if a loan is active. If it can pay it off entirely by the installment it does it.
    // Else pays installment or if no loan just transfers the money to bank balance.
    // Renders the textboxes to match.
    static transferToBank() {
        const installment = (payBalance * INSTALLMENT_PERCENTAGE);

        if (loan > (installment)) {
            //deduct 10% to pay off loan.
            loan -= installment;
            payBalance -= installment;
            //deposit rest
            bankBalance += payBalance;
            payBalance = 0;
        }
        else if (loan > 0 && loan < installment) {
            //if loan is smaller than installment pay it off entirely.
            alert(`Paid off remaining loan of (${toCurrency(loan)}).`);
            payBalance -= loan;
            bankBalance += payBalance;
            payBalance = 0; loan = 0; // inline to save space.
            Renderer.hideLoanContainer();
            Renderer.hideRepayLoanButton();
        }
        else {
            //if no loans just transfer money's
            bankBalance += payBalance;
            payBalance = 0;
        }
        Renderer.updatePayBalance();
        Renderer.updateBankBalance();
        Renderer.updateLoanOutstanding();

    }
    // A lot of errorchecks are run first and Alerts display to provide useful information.
    // Checks that loan is not higher than 50% of total bank balance. (and that loan is not an unvalid one)
    // if no errors adds loan amount to loan outstanding and your bank balance.
    // Renders textboxes to match.
    static promptLoan() {
        if (loan > 0) return alert("You can only have one loan at a time.");
        const loanAsk = prompt(`How much would you like to loan? You can loan a maximum of ${toCurrency(bankBalance * MAX_LOAN_PERCENTAGE)}.`)
        if (loanAsk === null) return;

        if (loanAsk.includes(',')) return alert("Use '.' (dot) instead of ',' (comma)");
        if (!Helpers.isNumeric(loanAsk)) return alert("Error! Try again with valid values. e.g. '2.42', '100', '200.49', '359.24'.");
        if (loanAsk <= 0) return alert(`Loan cannot be smaller than ${toCurrency(1)}!`);

        if (!(loanAsk <= (bankBalance * MAX_LOAN_PERCENTAGE))) {
            return alert(`Cannot loan more than half of your Bank balance! Your maximum loan capacity is: ${toCurrency(bankBalance * MAX_LOAN_PERCENTAGE)}`);
        }
        else {
            alert(`Loan of ${toCurrency(loanAsk)} has been granted and deposited into your bank.`);
            loan += parseFloat(loanAsk);
            bankBalance += parseFloat(loanAsk);
            Renderer.updateBankBalance();
            Renderer.updateLoanOutstanding();
            Renderer.showLoanContainer();
            Renderer.showRepayLoanButton();
        }
    }
    // Pays loan with pay. checks which one is larger to know if loan is paid off.
    // Renders textboxes to match and hides loancontainer and repayloan button.
    static repayLoan() {
        if (payBalance === 0) return alert("You need to go earn some money's before you can do that.");
        if (payBalance <= loan) {
            loan -= payBalance;
            payBalance = 0;
            Renderer.updateLoanOutstanding();
            Renderer.updatePayBalance();
        } else if (payBalance >= loan) {
            payBalance -= loan;
            loan = 0;
            Renderer.updateLoanOutstanding();
            Renderer.updatePayBalance();
            Renderer.hideLoanContainer();
            Renderer.hideRepayLoanButton();
            return alert("You have now paid off your loan.")
        }
    }
    // Checks if there are enough bank balance to buy the laptop selected.
    static buyNow() {
        const laptop = laptops.find(x => x.id == Elements.LaptopsListElement.selectedIndex + 1);
        if (bankBalance < laptop.price) {
            return alert("You need to go work some more to be able to buy that shiny new computer.")
        }
        else {
            bankBalance -= laptop.price;
            Renderer.updateBankBalance();
            return alert(`You're now the proud owner of a ${laptop.title}. ${toCurrency(laptop.price)} has been deducted from your bank account.`)
        }
    }
    // Updates the selectedLaptop to matching Id when another laptop is selected in the listbox
    static updateLaptopSelection(event) {
        Renderer.updateLaptopSelection((event.target.selectedIndex + 1))
    }
    // A method that is run when DOM content is loaded.
    // Mainly used to clear stuff. Also gets the laptops with GetLaptops call then selects the first one in listbox
    static async onInitialize() {
        Renderer.clearPayBalance();
        Renderer.clearLoanOutstanding();
        Renderer.clearBankBalance();
        Renderer.hideLoanContainer();
        await GetLaptops();
        Elements.LaptopsListElement.selectedIndex = 0;
        await Renderer.updateLaptopSelection(1);
        payBalance = 0; bankBalance = 0; loan = 0; // inline to save space.
    }
}

//Eventlisteners that call Events in the Event class
Elements.GetPaidButtonElement.addEventListener('click', Events.getPaid);
Elements.BankTransferButtonElement.addEventListener('click', Events.transferToBank);
Elements.GetLoanElement.addEventListener('click', Events.promptLoan);
Elements.RepayLoanButtonElement.addEventListener('click', Events.repayLoan);
Elements.LaptopsListElement.addEventListener('change', Events.updateLaptopSelection);
Elements.BuyNowButtonElement.addEventListener('click', Events.buyNow)
window.addEventListener('DOMContentLoaded', Events.onInitialize);
